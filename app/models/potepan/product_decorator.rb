Spree::Product.class_eval do
  scope :include_master, -> { includes(master: [:images, :default_price]) }
  scope :join_taxons, -> { joins(:taxons) }
  scope :join_taxons_and_search, ->(ids) { join_taxons.where(spree_taxons: { id: ids }) }
  scope :omit_product, ->(product_id) { where.not(id: product_id) }
  scope :available_since, ->(since_date) { where('available_on >= ?', since_date) }
end
