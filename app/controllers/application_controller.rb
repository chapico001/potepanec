class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  def render_404
    render file: "#{Rails.root}/public/404.html", status: :not_found, layout: false, content_type: 'text/html'
  end
end
