class Potepan::TopController < ApplicationController
  def index
    @products = Spree::Product.include_master.available_since(PotepanEc::ConstSettings.new_arraivals_period)
  end
end
