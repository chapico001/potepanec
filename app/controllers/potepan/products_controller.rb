class Potepan::ProductsController < ApplicationController
  include Spree::Core::ControllerHelpers::Pricing
  include Spree::Core::ControllerHelpers::Store

  rescue_from ActiveRecord::RecordNotFound, with: :render_404

  def show
    @product = Spree::Product.find(params[:id])
    @properties = Spree::Property.joins(:products).where(spree_products: { id: @product.id })

    # 画面に表示するproductと同じカテゴリーのプロダクトを取得
    # 重複をなくし、表示商品は除く
    @same_taxon_products = Spree::Product.include_master.join_taxons_and_search(@product.taxons.ids).
      omit_product(@product.id).distinct.limit(PotepanEc::ConstSettings.related_products_count)
  end

end
