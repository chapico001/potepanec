class Potepan::CategoriesController < ApplicationController

  rescue_from ActiveRecord::RecordNotFound, with: :render_404

  def show
    @taxon = Spree::Taxon.find(params[:id])
    # taxon(子カテゴリーを含む)に紐づくproductを取得。
    @products = Spree::Product.include_master.join_taxons_and_search(@taxon.self_and_descendants.ids)
    @taxonomies = Spree::Taxonomy.includes(root: :children)
  end

end
