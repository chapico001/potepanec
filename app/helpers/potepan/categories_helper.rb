module Potepan::CategoriesHelper

  # taxonに紐づくpuroductの件数を取得する。
  def taxons_product_count(taxon)
    taxon.self_and_descendants.map { |t| t.products.count }.sum
  end

end