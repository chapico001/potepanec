module ApplicationHelper

  # 商品名を含むタイトルを返す。
  def full_title(page_title = '')
    base_name = "BIGBAG Store"
    if page_title.empty?
      base_name
    else
      page_title + "-" + base_name
    end
  end

  def top_page?
    controller.controller_name == 'top'
  end
end
