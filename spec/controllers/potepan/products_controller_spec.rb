require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  let(:product) { create(:product) }

  describe "GET #product" do
    context "normal" do
      before { get :show, params: { id: product.id } }

      it "returns http success" do
        expect(response).to have_http_status(:success)
      end

      it "assigns product" do
        expect(assigns(:product)).to eq product
      end

      it "renders the :show template" do
        expect(response).to render_template :show
      end
    end

    context "normal create property" do
      let!(:property) { create(:property) }
      let!(:product_property) { create(:product_property,
                                       product: product,
                                       property: property) }

      before do
        get :show, params: { id: product.id }
      end

      it "assigns properties" do
        expect(assigns(:properties)).to match_array product.properties
      end
    end

    # 関連商品についてのテスト
    context "when product has same taxon products" do
      # 別カテゴリーの同一商品を作成する。（重複を省く処理の確認のため）
      # リクエストに使用する商品は関連商品に含まないため別で作成する。
      # 定数で設定した商品数のみがassignsされていること
      products_count = PotepanEc::ConstSettings.related_products_count
      let(:taxons) { create_list(:taxon, 2) }
      let!(:product_1) { create(:product, taxons: taxons) }
      let!(:products_with_const_value) { create_list(:product, products_count, taxons: taxons) }
      let!(:product_no_assigns) { create(:product, taxons: taxons) }

      before do
        get :show, params: { id: product_1.id }
      end

      it "same_taxon_products count equal const value" do
        expect(assigns(:same_taxon_products).count).to eq products_count
      end

      it "assigns same_taxon_products" do
        # 表示商品と重複を除く結果を期待する。
        expect(assigns(:same_taxon_products)).to match_array products_with_const_value
      end

      it "no assigns out of const value" do
        # 定数で設定された数以上は含まれない
        expect(assigns(:same_taxon_products)).not_to include product_no_assigns
      end
    end

    context "when product has no same taxon products" do
      # リクエストに使用する商品は関連商品に含まないため別で作成する。
      # 異なるtaxonの商品のみ存在する（関連商品が存在しない)
      products_count = PotepanEc::ConstSettings.related_products_count
      let(:taxons) { create_list(:taxon, 1) }
      let(:other_taxons) { create_list(:taxon, 1) }
      let!(:product_1) { create(:product, taxons: taxons) }
      let!(:products_with_other_taxons) { create_list(:product, products_count, taxons: other_taxons) }

      before do
        get :show, params: { id: product_1.id }
      end

      it "same_taxon_products count equal zero" do
        expect(assigns(:same_taxon_products).count).to eq 0
      end

      it "no assigns same_taxon_products" do
        expect(assigns(:same_taxon_products)).not_to match_array products_with_other_taxons
      end
    end

    context "error" do
      before { get :show, params: { id: "a" } }

      it "returns http not found" do
        expect(response).to have_http_status("404")
      end
    end
  end
end
