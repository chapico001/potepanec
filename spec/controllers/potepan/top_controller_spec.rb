require 'rails_helper'

RSpec.describe Potepan::TopController, type: :controller do
  describe "GET #index" do
    let(:available_on) { PotepanEc::ConstSettings.new_arraivals_period }

    before { get :index }

    it "returns http success" do
      expect(response).to have_http_status(:success)
    end

    # 新着商品を取得
    describe "new arrivals" do
      let!(:products1) { create_list(:product, 1, available_on: available_on) }
      let!(:products2) { create_list(:product, 1, available_on: available_on - 1.day) }

      subject(:products) { assigns(:products) }

      it "assigns specified period" do
        expect(products).to eq products1
      end

      it "not assigns without specified period" do
        expect(products).to_not include products2
      end
    end
  end
end
