require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do

  let(:products) { create_list(:product, 5) }
  let(:taxon_with_5_products) do
    create(:taxon, products: products)
  end

  describe "GET #categories" do
    context "normal" do
      before { get :show, params: { id: taxon_with_5_products.id } }

      it "returns http success" do
        expect(response).to have_http_status(:success)
      end

      it "assigns taxon" do
        expect(assigns(:taxon)).to eq taxon_with_5_products
      end

      it "assigns product" do
        expect(assigns(:products)).to match_array taxon_with_5_products.products
      end

      it "assigns taxonomies" do
        expect(assigns(:taxonomies)).to match_array taxon_with_5_products.taxonomy
      end


      it "renders the :show template" do
        expect(response).to render_template :show
      end
    end

    context "error" do
      before { get :show, params: { id: "abc" } }

      it "returns http not found" do
        expect(response).to have_http_status("404")
      end
    end
  end
end
