require 'rails_helper'

RSpec.feature "Potepan::Tops", type: :feature do

  # 新着対象内の日付
  given!(:product1) do
    create(:product,
           name: "product_name_1",
           price: "77.77",
           available_on: PotepanEc::ConstSettings.new_arraivals_period)
  end
  # 新着対象外の日付
  given!(:product2) do
    create(:product,
           name: "product_name_2",
           price: "88.88",
           available_on: PotepanEc::ConstSettings.new_arraivals_period - 1.day)
  end

  # メイン商品
  scenario "top view" do
    visit "/potepan/"

    # 新着商品
    expect(page).to have_link "product_name_1", href: "/potepan/products/#{product1.id}"
    expect(page).to have_content "$77.77"

    # 新着商品でないもの(表示されていないこと)
    expect(page).to have_no_link "product_name_2", href: "/potepan/products/#{product2.id}"
    expect(page).to have_no_content "$88.88"
  end

  #新着商品リンククリック
  scenario "new arrivals link click" do
    visit "/potepan/"

    click_link "product_name_1"

    expect(page).to have_current_path "/potepan/products/#{product1.id}"
    expect(page).to have_title "product_name_1-BIGBAG Store"
    expect(page).to have_content "product_name_1"
  end
end
