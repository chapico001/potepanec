require 'rails_helper'

RSpec.feature "Potepan::Categories", type: :feature do
  Spree::Config[:currency] = "USD"

  given(:taxonomy) { create(:taxonomy, name: 'taxonomy_name') }
  given(:root) { taxonomy.root }

  given(:product_1) { create(:product, name: "product_name_1", price: 77.77) }
  given(:product_2) { create(:product, name: "product_name_2", price: 88.88) }
  given(:product_3) { create(:product, name: "product_name_3", price: 99.99) }

  given(:taxon) do
    products = []
    products << product_1
    products << product_2
    products << product_3
    create(:taxon, name: "taxon_categories", products: products, parent: root, taxonomy: taxonomy)
  end

  scenario "categories view test", js: true do
    visit "/potepan/categories/#{taxon.id}"

    # TITLE
    expect(page).to have_title "taxon_categories-BIGBAG Store"

    # LIGHT SECTION
    expect(page).to have_content "TAXON_CATEGORIES"

    # サイドバーのカテゴリー(taxonomy)
    expect(page).to have_link "taxonomy_name", href: "javascript:;"

    # カテゴリークリック(taxon表示)
    click_link "taxonomy_name"

    # サイドバーカテゴリー(taxon)
    expect(page).to have_link "taxon_categories", href: "/potepan/categories/#{taxon.id}"
    expect(page).to have_content "(3)"

    # 表示商品
    expect(page).to have_link "product_name_1", href: "/potepan/products/#{product_1.id}"
    expect(page).to have_content "$77.77"
    expect(page).to have_link "product_name_2", href: "/potepan/products/#{product_2.id}"
    expect(page).to have_content "$88.88"
    expect(page).to have_link "product_name_3", href: "/potepan/products/#{product_3.id}"
    expect(page).to have_content "$99.99"
  end

  #関連商品リンククリック
  scenario "category link click" do
    visit "/potepan/categories/#{taxon.id}"

    click_link "product_name_1"

    expect(page).to have_current_path "/potepan/products/#{product_1.id}"
    expect(page).to have_title "product_name_1-BIGBAG Store"
    expect(page).to have_content "product_name_1"
  end
end
