require 'rails_helper'

RSpec.feature "Potepan::Products", type: :feature do
  Spree::Config[:currency] = "USD"

  given(:product) { create(:product, name: "product_name", price: 11.11) }
  given(:product_1) { create(:product, name: "same_category_name1", price: 44.44) }
  given(:product_2) { create(:product, name: "same_category_name2", price: 55.55) }
  given(:product_3) { create(:product, name: "same_category_name3", price: 66.66) }
  given(:product_4) { create(:product, name: "same_category_name4", price: 77.77) }
  given(:product_5) { create(:product, name: "same_category_name5", price: 88.88) }


  given(:property) { create(:property,
                            presentation: "property_presentation") }
  given!(:product_property) { create(:product_property,
                                     product: product,
                                     property: property,
                                     value: "product_property_value") }
  # 同一カテゴリーの商品を作成
  given!(:same_taxon_products) do
    products = []
    products << product
    products << product_1
    products << product_2
    products << product_3
    products << product_4
    products << product_5
    create(:taxon, products: products)
  end

  # メイン商品
  scenario "main products view" do
    visit "/potepan/products/#{product.id}"

    expect(page).to have_title "product_name-BIGBAG Store"
    expect(page).to have_content "product_name"
    expect(page).to have_content "property_presentation"
    expect(page).to have_content "product_property_value"
    expect(page).to have_content "$11.11"
  end

  #同じカテゴリーの商品
  scenario "same category products view" do
    visit "/potepan/products/#{product.id}"

    expect(page).to have_link "same_category_name1", href: "/potepan/products/#{product_1.id}"
    expect(page).to have_content "$44.44"
    expect(page).to have_link "same_category_name2", href: "/potepan/products/#{product_2.id}"
    expect(page).to have_content "$55.55"
    expect(page).to have_link "same_category_name3", href: "/potepan/products/#{product_3.id}"
    expect(page).to have_content "$66.66"
    expect(page).to have_link "same_category_name4", href: "/potepan/products/#{product_4.id}"
    expect(page).to have_content "$77.77"

    # 関連商品は４件以上出ないことの確認
    expect(page).to have_no_link "same_category_name5", href: "/potepan/products/#{product_5.id} "
    expect(page).to have_no_content "$88.88"
  end

  #関連商品リンククリック
  scenario "same category link click" do
    visit "/potepan/products/#{product.id}"

    click_link "same_category_name1"

    expect(page).to have_current_path "/potepan/products/#{product_1.id}"
    expect(page).to have_title "same_category_name1-BIGBAG Store"
    expect(page).to have_content "same_category_name1"
  end
end
