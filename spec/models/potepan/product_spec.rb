require 'rails_helper'

RSpec.describe Spree::Product, type: :model do

  describe "scope test" do
    describe ".join_taxons_and_search" do
      context "when taxons with products" do
        let(:taxons) { create_list(:taxon, 1, id: 201) }
        let!(:products) { create_list(:product, 1, taxons: taxons) }

        # joinすることでtaxonのidからproductsを参照できること
        it "exist products" do
          expect(Spree::Product.join_taxons_and_search(201)).to eq products
        end
      end
    end

    describe ".omit_product" do
      context "when set product's id" do
        let!(:product) { create(:product) }
        let!(:products) { create_list(:product, 1) }

        # パラメータで指定したidのproductを省いていること
        it "not exist setted id's product" do
          expect(Spree::Product.omit_product(product.id)).to eq products
        end
      end
    end

    describe ".available_since" do
      context "when set date" do
        let(:available_on) { PotepanEc::ConstSettings.new_arraivals_period }
        let!(:product_1) { create(:product, available_on: available_on) }
        let!(:product_2) { create(:product, available_on: available_on + 1.day) }
        let!(:product_3) { create(:product, available_on: available_on - 1.day) }

        # パラメータの日付以内のproductを取得すること
        it "since params'date " do
          products = []
          products << product_1
          products << product_2
          expect(Spree::Product.available_since(available_on)).to eq products
          expect(Spree::Product.available_since(available_on)).not_to include product_3
        end
      end
    end
  end
end
