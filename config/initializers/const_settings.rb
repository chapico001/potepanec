module PotepanEc
  class ConstSettings < Settingslogic
    source "#{Rails.root}/config/potepanec_const.yml"
    namespace Rails.env
  end
end